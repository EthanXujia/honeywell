//
//  NewsViewController.m
//  HoneyWell
//
//  Created by Jia.xu on 14-8-4.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import "NewsViewController.h"
#import "QRScanningViewController.h"

@interface NewsViewController ()

@end

@implementation NewsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = NSLocalizedString(TITLE_NEWS, nil);
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchSettingItem:(id)sender
{
    [UIView animateWithDuration:.25f
                          delay:.0f
                        options:UIViewAnimationOptionAllowUserInteraction|UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         _sliderMenuView.frame = _sliderMenuView.hidden ? _sliderMenuTargetFrame : _sliderMenuOriginalFrame;
//                         _collectionView.frame = _sliderMenuView.hidden ? CGRectMake(_sliderMenuView.frame.size.width, 0, _collectionView.frame.size.width, _collectionView.frame.size.height) : CGRectMake(0, 0, _collectionView.frame.size.width, _collectionView.frame.size.height);
                         _sliderMenuView.hidden = NO;
                     }
                     completion:^(BOOL finished){
                         if (_sliderMenuView.frame.origin.x < 0) {
                             _sliderMenuView.hidden = YES;
                         }
                     }];
}

@end
