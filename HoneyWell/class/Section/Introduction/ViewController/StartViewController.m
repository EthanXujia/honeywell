//
//  StartViewController.m
//  HoneyWell
//
//  Created by Jia.xu on 14-8-4.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import "StartViewController.h"

#define PAGE_NUMBER 3

@interface StartViewController ()
<UIScrollViewDelegate>
{
    UIScrollView *_scrollView;
    UIPageControl *_pageControl;
}
@end

@implementation StartViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    _scrollView.delegate = self;
    _scrollView.pagingEnabled = YES;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.backgroundColor = [UIColor clearColor];
    
    CGSize scrollViewSize = CGSizeMake(self.view.frame.size.width * PAGE_NUMBER, self.view.frame.size.height);
    [_scrollView setContentSize:scrollViewSize];
    [self.view addSubview:_scrollView];
    
    _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width/2, 30)];
    _pageControl.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height-_pageControl.frame.size.height);
    _pageControl.numberOfPages = PAGE_NUMBER;
    _pageControl.currentPage = 0;
    _pageControl.backgroundColor = [UIColor clearColor];
    [_pageControl addTarget:self action:@selector(changedPages:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:_pageControl];
    
    NSArray *infos = [NSArray arrayWithObjects:@"Air Cleaner", @"PM2.5", @"QR Scanning", nil];
    CGFloat posx = 0;
    for (int i=0; i<PAGE_NUMBER; i++) {
        UILabel *info = [[UILabel alloc] initWithFrame:CGRectMake(posx, 0, _scrollView.frame.size.width, _scrollView.frame.size.height)];
        info.backgroundColor = [UIColor clearColor];
        info.textColor = [UIColor blueColor];
        info.font = [UIFont systemFontOfSize:30];
        info.text = infos[i];
        info.textAlignment = NSTextAlignmentCenter;
        [_scrollView addSubview:info];
        posx+=_scrollView.frame.size.width;
    }
    
    UIButton *skipBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    skipBtn.frame = CGRectMake(0, 0, 200, 50);
    skipBtn.center = CGPointMake(posx-self.view.frame.size.width/2, _pageControl.center.y-80);
    [skipBtn setTitle:@"Skip" forState:UIControlStateNormal];
    [skipBtn setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
    [skipBtn setBackgroundColor:[UIColor blueColor]];
    [skipBtn addTarget:self action:@selector(touchSkip:) forControlEvents:UIControlEventTouchUpInside];
    [_scrollView addSubview:skipBtn];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchSkip:(id)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 
                             }];
}

- (void)changedPages:(id)sender
{
    [_pageControl updateCurrentPageDisplay];
    NSInteger page = _pageControl.currentPage;
    CGFloat x = page*_scrollView.frame.size.width;
    [_scrollView setContentOffset:CGPointMake(x, 0) animated:YES];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int index = fabs(_scrollView.contentOffset.x) / _scrollView.frame.size.width;
    _pageControl.currentPage = index;
}


@end
