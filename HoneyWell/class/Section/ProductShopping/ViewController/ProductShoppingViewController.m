//
//  ProductShoppingViewController.m
//  HoneyWell
//
//  Created by Jia.xu on 14-8-6.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import "ProductShoppingViewController.h"
#import "ProductTipCell.h"
#import "ProductCell.h"

@interface ProductShoppingViewController ()
<UITableViewDataSource, UITableViewDelegate>
{
    UITableView *_tableView;
    ProductTipCell *_tipCell;
    CGFloat _cellHeight;
}
@end

@implementation ProductShoppingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = NSLocalizedString(TITLE_PRODUCT_SHOPPING, nil);
    
    NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([ProductTipCell class]) owner:self options:nil];
    _tipCell = [nibs objectAtIndex:0];
    
    nibs = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([ProductCell class]) owner:self options:nil];
    UITableViewCell *cell = [nibs objectAtIndex:0];
    _cellHeight = cell.frame.size.height;
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, _heightFromNavBarToBottom) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_tableView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma - mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return _tipCell.frame.size.height;
    }
    return _cellHeight;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return nil;
    }
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString *string = nil;
    [Global openApplication:string];
}

#pragma - mark UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return TEST_PRODUCTS_NUMBER+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        _tipCell.selectionStyle = UITableViewCellSelectionStyleNone;
        return _tipCell;
    }else{
        static NSString *key = @"ProductCell";
        ProductCell *cell = [tableView dequeueReusableCellWithIdentifier:key];
        if (cell == nil) {
            NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([ProductCell class]) owner:self options:nil];
            cell = [nibs objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
        }
        NSInteger index = (NSInteger)indexPath.row;
        cell.mProductNumber.text = [NSString stringWithFormat:@"%ld", index];
        cell.mProductName.text = [NSString stringWithFormat:@"购买%ld号过滤网", index];
        return cell;
    }
}

@end
