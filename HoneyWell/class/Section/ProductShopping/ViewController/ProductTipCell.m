//
//  ProductTipCell.m
//  HoneyWell
//
//  Created by Jia.xu on 14-8-6.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import "ProductTipCell.h"
#import "ProductManualsReaderViewController.h"

@implementation ProductTipCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)touchManual:(id)sender
{
    ProductManualsReaderViewController *vc = [[ProductManualsReaderViewController alloc] init];
    vc.mManualType = kManualSetup;
    [[Global getNavigationController] pushViewController:vc animated:YES];
}

@end
