//
//  ProductCell.h
//  HoneyWell
//
//  Created by Jia.xu on 14-8-6.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *mProductName;
@property (nonatomic, strong) IBOutlet UILabel *mProductNumber;
@property (nonatomic, strong) IBOutlet UIImageView *mProductImage;

@end
