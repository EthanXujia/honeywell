//
//  FeedbackViewController.m
//  HoneyWell
//
//  Created by Jia.xu on 14-8-6.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import "FeedbackViewController.h"

@interface FeedbackViewController ()
<UITextViewDelegate>
{
    UITextView *_textView;
}
@end

@implementation FeedbackViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = NSLocalizedString(TITLE_ADVISE_SUGGESTION, nil);
    
    UIButton *submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    submitBtn.frame = CGRectMake(0, 0, 100, 30);
    submitBtn.center = CGPointMake(self.view.frame.size.width/2, _heightFromNavBarToBottom-20-submitBtn.frame.size.height/2);
    submitBtn.backgroundColor = UIColorFromRGB(0xaaaaaa);
    [submitBtn setTitle:NSLocalizedString(TITLE_SUBMIT, nil) forState:UIControlStateNormal];
    [submitBtn addTarget:self action:@selector(touchSubmit:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:submitBtn];
    
    _textView = [[UITextView alloc] initWithFrame:CGRectMake(20, 20, self.view.frame.size.width-20*2, submitBtn.frame.origin.y-20-20)];
    _textView.layer.masksToBounds = YES;
    _textView.layer.cornerRadius = 5.0f;
    _textView.layer.borderWidth = 1.0f;
    _textView.layer.borderColor = [UIColor brownColor].CGColor;
    _textView.delegate = self;
    _textView.keyboardType = UIKeyboardTypeDefault;
    _textView.textColor = [UIColor lightGrayColor];
    _textView.text = NSLocalizedString(TITLE_PLEASE_INPUT_YOUR_SUGGESTION, nil);
    [self.view addSubview:_textView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchSubmit:(id)sender
{
    // submit the feedback and return Home View
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UIView *view in self.view.subviews) {
        if (![view isKindOfClass:[UITextView class]]) {
            [_textView resignFirstResponder];
            break;
        }
    }
}

#pragma mark - UITextViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:NSLocalizedString(TITLE_PLEASE_INPUT_YOUR_SUGGESTION, nil)]) {
        textView.textColor = [UIColor blackColor];
        textView.text = @"";
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView.text.length == 0) {
        _textView.textColor = [UIColor lightGrayColor];
        _textView.text = NSLocalizedString(TITLE_PLEASE_INPUT_YOUR_SUGGESTION, nil);
    }
}


@end
