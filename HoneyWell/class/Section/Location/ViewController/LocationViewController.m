//
//  LocationViewController.m
//  HoneyWell
//
//  Created by Jia.xu on 14-8-4.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import "LocationViewController.h"
#import "LocationManager.h"

#define CELL_LINE_TAG 9
#define CELL_SCROLLVIEW_TAG 10

@interface LocationViewController ()
<UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate>
{
    CGFloat _cellHeight;
    NSArray *_resultArray;
    NSArray *_recommendCityArray;
    UIButton *_locButton;
    UITableView *_tableView;
    UISearchBar *_searchBar;
}

@end

@implementation LocationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = NSLocalizedString(TITLE_ADD_CITY, nil);
    
    NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"CityList" ofType:@"plist"]];
    NSInteger count = [[DataManager shareDataManager] countForEntityClass:[CityEntity class] sortDescriptorsOrNil:nil];
    if (count == 0) {
        NSArray *array = dic[@"citylist"];
        for (int i=0; i<[array count]; i++) {
            NSDictionary *cityDic = [array objectAtIndex:i];
            CityEntity *city = [[DataManager shareDataManager] createManagedObject:[CityEntity class]];
            city.cityName = cityDic[@"cityName"];
            city.cityCode = cityDic[@"cityCode"];
        }
        [[DataManager shareDataManager] saveManagedObjects];
    }
    
    _recommendCityArray = dic[@"recommendCities"];
    
    _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    _searchBar.delegate = self;
    _searchBar.keyboardType = UIKeyboardTypeDefault;
    _searchBar.placeholder = NSLocalizedString(TITLE_SEARCH, nil);
    [self.view addSubview:_searchBar];
    
    _locButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _locButton.frame = CGRectMake(0, 0, 100, 30);
    _locButton.center = CGPointMake(self.view.frame.size.width/2, _heightFromNavBarToBottom-20-30/2);
    _locButton.backgroundColor = [UIColor lightGrayColor];
    [_locButton addTarget:self action:@selector(touchLocation:) forControlEvents:UIControlEventTouchUpInside];
    [_locButton setTitle:NSLocalizedString(TITLE_LOCATIOIN, nil) forState:UIControlStateNormal];
    [self.view addSubview:_locButton];
    
    _cellHeight = 44;
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _searchBar.frame.origin.y+_searchBar.frame.size.height, self.view.frame.size.width, _heightFromNavBarToBottom-_searchBar.frame.size.height/*-_locButton.frame.size.height-20-20*/) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma - mark UISearchBarDelegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    _searchBar.showsCancelButton = YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    _searchBar.showsCancelButton = NO;
    _resultArray = [DataManager arrayForEntity:[CityEntity class] sortDescriptorsOrNil:nil predicateOrNil:[NSPredicate predicateWithFormat:@"SELF.cityName CONTAINS %@", searchBar.text]];
    [_tableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    _resultArray = [DataManager arrayForEntity:[CityEntity class] sortDescriptorsOrNil:nil predicateOrNil:[NSPredicate predicateWithFormat:@"SELF.cityName CONTAINS %@", searchText]];
    [_tableView reloadData];
}

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    [searchBar resignFirstResponder];
}

#pragma - mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return _cellHeight;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!_resultArray || [_resultArray count] == 0) {
        return nil;
    }
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CityEntity *city = _resultArray[indexPath.row];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        NSArray *array = [DataManager arrayForEntity:[CityWeatherEntity class] sortDescriptorsOrNil:nil predicateOrNil:[NSPredicate predicateWithFormat:@"SELF.cityName == %@", city.cityName]];
        if (array && [array count] != 0) {
            CityWeatherEntity *cityWeather = array.firstObject;
            [DataManager deleteManagedInstance:cityWeather];
            [DataManager saveManagedInstances];
        }
    }else{
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        CityWeatherEntity *cityWeather = [DataManager createManagedInstance:[CityWeatherEntity class]];
        cityWeather.cityName = city.cityName;
        cityWeather.cityCode = city.cityCode;
        [DataManager saveManagedInstances];
    }
    [tableView reloadData];
}

#pragma - mark UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!_resultArray || [_resultArray count] == 0) {
        return 1;
    }
    return [_resultArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!_resultArray || [_resultArray count] == 0) {
        static NSString *keyLoc = @"recommendCityCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:keyLoc];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:keyLoc];
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
        }
        UILabel *line = (UILabel*)[cell viewWithTag:CELL_LINE_TAG];
        if (!line) {
            line = [[UILabel alloc] initWithFrame:CGRectMake(10, cell.frame.size.height-2, cell.frame.size.width-10*2, 1)];
            line.tag = CELL_LINE_TAG;
            line.backgroundColor = [UIColor lightGrayColor];
            [cell.contentView addSubview:line];
        }
        cell.accessoryType = UITableViewCellAccessoryNone;
        UIScrollView *scrollView = (UIScrollView*)[cell viewWithTag:CELL_SCROLLVIEW_TAG];
        if (!scrollView) {
            scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height-2)];
            scrollView.tag = CELL_SCROLLVIEW_TAG;
            [cell.contentView addSubview:scrollView];
            CGFloat wid = self.view.frame.size.width/4;
            if ([_recommendCityArray count] > 4) {
                scrollView.contentSize = CGSizeMake([_recommendCityArray count]*wid, scrollView.frame.size.height);
            }else {
                wid = self.view.frame.size.width/[_recommendCityArray count];
            }
            for (int i=0; i<[_recommendCityArray count]; i++) {
                NSDictionary *dic = _recommendCityArray[i];
                UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                btn.tag = i;
                btn.frame = CGRectMake(i*wid, 0, wid, scrollView.frame.size.height);
                btn.backgroundColor = UIColorFromRGB(0x112233);
                [btn setTitle:dic[@"cityName"] forState:UIControlStateNormal];
                [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [btn addTarget:self action:@selector(touchRecommendCity:) forControlEvents:UIControlEventTouchUpInside];
                [scrollView addSubview:btn];
            }
        }
        return cell;
    }
    
    static NSString *keyLoc = @"cityCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:keyLoc];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:keyLoc];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    UILabel *line = (UILabel*)[cell viewWithTag:CELL_LINE_TAG];
    if (!line) {
        line = [[UILabel alloc] initWithFrame:CGRectMake(10, cell.frame.size.height-2, cell.frame.size.width-10*2, 1)];
        line.tag = CELL_LINE_TAG;
        line.backgroundColor = [UIColor lightGrayColor];
        [cell.contentView addSubview:line];
    }
    CityEntity *city = [_resultArray objectAtIndex:indexPath.row];
    NSInteger count = [DataManager countForEntityClass:[CityWeatherEntity class]
                                  sortDescriptorsOrNil:nil
                                        predicateOrNil:[NSPredicate predicateWithFormat:@"SELF.cityName == %@", city.cityName]];
    cell.accessoryType = count == 0 ? UITableViewCellAccessoryNone : UITableViewCellAccessoryCheckmark;
    cell.textLabel.text = city.cityName;
    return cell;
}

- (void)touchRecommendCity:(UIButton*)btn
{
    NSInteger index = btn.tag;
    NSDictionary *dic = _recommendCityArray[index];
    NSInteger count = [DataManager countForEntityClass:[CityWeatherEntity class]
                                  sortDescriptorsOrNil:nil
                                        predicateOrNil:[NSPredicate predicateWithFormat:@"SELF.cityName == %@", dic[@"cityName"]]];
    if (count == 0) {
        CityWeatherEntity *cityWeather = [DataManager createManagedInstance:[CityWeatherEntity class]];
        cityWeather.cityName = dic[@"cityName"];
        cityWeather.cityCode = dic[@"cityCode"];
    }else{
        NSArray *array = [DataManager arrayForEntity:[CityWeatherEntity class] sortDescriptorsOrNil:nil predicateOrNil:[NSPredicate predicateWithFormat:@"SELF.cityName == %@", dic[@"cityName"]]];
        if (array && [array count] != 0) {
            CityWeatherEntity *cityWeather = array.firstObject;
            [DataManager deleteManagedInstance:cityWeather];
        }
    }
    [DataManager saveManagedInstances];
}

- (void)touchLocation:(id)sender
{
    [[LocationManager shareLocationManager] startUpdateLocation];
}

@end
