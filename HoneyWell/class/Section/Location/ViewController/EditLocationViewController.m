//
//  EditLocationViewController.m
//  HoneyWell
//
//  Created by Jia.xu on 14-8-7.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import "EditLocationViewController.h"
#import "LocationViewController.h"

#define CELL_LINE_TAG   9
#define SWITCH_TAG  999
#define DELETE_BUTTON_TAG   10

@interface EditLocationViewController ()
<UITableViewDataSource, UITableViewDelegate>
{
    UITableView *_tableView;
    NSArray *_citiesArray;
    CGFloat _cellHeight;
}

@end

@implementation EditLocationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _citiesArray = [[DataManager shareDataManager] arrayForEntities:[CityWeatherEntity class] sortDescriptorsOrNil:nil];
    if (_tableView) {
        [_tableView reloadData];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = NSLocalizedString(TITLE_EDIT_LOCATION, nil);
    
    _cellHeight = 44;
    
    _citiesArray = [[DataManager shareDataManager] arrayForEntities:[CityWeatherEntity class] sortDescriptorsOrNil:nil];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, _heightFromNavBarToBottom) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    UIBarButtonItem *addLocItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(touchAddLocationItem:)];
    self.navigationItem.rightBarButtonItem = addLocItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchAddLocationItem:(id)sender
{
    LocationViewController *vc = [[LocationViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)changeSwitchValue:(UISwitch*)switchView;
{
    SettingEntity *entity = nil;
    NSArray *array = [DataManager arrayForEntity:[SettingEntity class] sortDescriptorsOrNil:nil];
    if (array && [array count] != 0) {
        entity = array.firstObject;
        entity.openGPS = [NSNumber numberWithBool:switchView.isOn];
        [DataManager saveManagedInstances];
    }
}

- (void)touchDelete:(UIButton*)btn
{
    NSInteger index = btn.tag-10;
    CityWeatherEntity *cityWeatherEntity =  _citiesArray[index];
    [DataManager deleteManagedInstance:cityWeatherEntity];
    [DataManager saveManagedInstances];
    _citiesArray = [[DataManager shareDataManager] arrayForEntities:[CityWeatherEntity class] sortDescriptorsOrNil:nil];
    [_tableView reloadData];
}

#pragma - mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return _cellHeight;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

#pragma - mark UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_citiesArray count]+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        static NSString *keyLoc = @"locCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:keyLoc];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:keyLoc];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.textLabel.text = [NSString stringWithFormat:@"当前位置: %@", @"上海"];
        UISwitch *switchView = (UISwitch*)[cell viewWithTag:SWITCH_TAG];
        if (!switchView) {
            switchView  = [[UISwitch alloc] init];
            switchView.tag = SWITCH_TAG;
            switchView.center = CGPointMake(cell.frame.size.width-switchView.frame.size.width/2-10, cell.frame.size.height/2);
            [switchView addTarget:self action:@selector(changeSwitchValue:) forControlEvents:UIControlEventValueChanged];
            [cell.contentView addSubview:switchView];
        }
        switchView.on = [Global isOpenGPS];
        UILabel *line = (UILabel*)[cell viewWithTag:CELL_LINE_TAG];
        if (!line) {
            line = [[UILabel alloc] initWithFrame:CGRectMake(10, cell.frame.size.height-2, cell.frame.size.width-10*2, 1)];
            line.tag = CELL_LINE_TAG;
            line.backgroundColor = [UIColor lightGrayColor];
            [cell.contentView addSubview:line];
        }
        return cell;
    }else{
        static NSString *keyCity = @"cityCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:keyCity];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:keyCity];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        CityWeatherEntity *city = _citiesArray[indexPath.row-1];
        cell.textLabel.text = city.cityName;
        UIButton *btn = (UIButton*)[cell viewWithTag:(indexPath.row-1+10)];
        if (!btn) {
            btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.tag = indexPath.row-1+10;
            btn.frame = CGRectMake(cell.frame.size.width-20-30/2, cell.frame.size.height/2-30/2, 30, 30);
            btn.backgroundColor = [UIColor lightGrayColor];
            [btn setTitle:@"X" forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(touchDelete:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:btn];
        }
        UILabel *line = (UILabel*)[cell viewWithTag:CELL_LINE_TAG];
        if (!line) {
            line = [[UILabel alloc] initWithFrame:CGRectMake(10, cell.frame.size.height-2, cell.frame.size.width-10*2, 1)];
            line.tag = CELL_LINE_TAG;
            line.backgroundColor = [UIColor lightGrayColor];
            [cell.contentView addSubview:line];
        }
        return cell;
    }
}

@end
