//
//  ProductManualViewController.m
//  HoneyWell
//
//  Created by Jia.xu on 14-8-5.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import "ProductManualViewController.h"
#import "ProductManualsReaderViewController.h"
#import "ProductManualCollectionViewCell.h"

#define COLLECTIONVIEW_COL  2
#define COLLECTIONVIEW_ROW  2

static NSString *kCollectionViewCell = @"kCollectionViewCell";

@interface ProductManualViewController ()
<UICollectionViewDataSource, UICollectionViewDelegate>
{
    UICollectionView *_collectionView;
    CGSize _collectionViewCellSize;
    NSArray *_manualArray;
}
@end

@implementation ProductManualViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = NSLocalizedString(TITLE_PRODUCT_MANUALS, nil);
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.minimumLineSpacing = 0;
    flowLayout.minimumInteritemSpacing = 0;
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    
    NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([ProductManualCollectionViewCell class]) owner:self options:nil];
    ProductManualCollectionViewCell *cell = [nibs objectAtIndex:0];
    _collectionViewCellSize = cell.frame.size;
    
    _manualArray = @[@"使用说明", @"安装维护", @"应用", @"注意事项"];
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, _heightFromNavBarToBottom) collectionViewLayout:flowLayout];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.showsHorizontalScrollIndicator = NO;
    _collectionView.pagingEnabled = YES;
    _collectionView.backgroundColor = [UIColor clearColor];
    [_collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([ProductManualCollectionViewCell class]) bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:kCollectionViewCell];
    [self.view addSubview:_collectionView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchSettingItem:(id)sender
{
    [UIView animateWithDuration:.25f
                          delay:.0f
                        options:UIViewAnimationOptionAllowUserInteraction|UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         _sliderMenuView.frame = _sliderMenuView.hidden ? _sliderMenuTargetFrame : _sliderMenuOriginalFrame;
                         _collectionView.frame = _sliderMenuView.hidden ? CGRectMake(_sliderMenuView.frame.size.width, 0, _collectionView.frame.size.width, _collectionView.frame.size.height) : CGRectMake(0, 0, _collectionView.frame.size.width, _collectionView.frame.size.height);
                         _sliderMenuView.hidden = NO;
                     }
                     completion:^(BOOL finished){
                         if (_sliderMenuView.frame.origin.x < 0) {
                             _sliderMenuView.hidden = YES;
                         }
                     }];
}

#pragma mark - UICollectionViewDataSource
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return _collectionViewCellSize;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    CGFloat space = (self.view.frame.size.width-2*_collectionViewCellSize.width)/3;
    return UIEdgeInsetsMake(space, space, 0, space);
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return COLLECTIONVIEW_ROW;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return COLLECTIONVIEW_COL;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ProductManualCollectionViewCell* cell =  (ProductManualCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kCollectionViewCell forIndexPath:indexPath];
    
    cell.layer.masksToBounds = YES;
    cell.layer.borderWidth = 1.0f;
    cell.layer.borderColor = [UIColor orangeColor].CGColor;
    cell.layer.cornerRadius = 5.0f;
    
    NSString *text = _manualArray[indexPath.section*COLLECTIONVIEW_ROW+indexPath.row];
    cell.mName.text = text;
    return cell;
}

#pragma - mark UICollectionViewDelegate
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    int index = indexPath.section*COLLECTIONVIEW_ROW+indexPath.row;
    ProductManualsReaderViewController *vc = [[ProductManualsReaderViewController alloc] init];
    vc.mManualType = kManualSetup;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
