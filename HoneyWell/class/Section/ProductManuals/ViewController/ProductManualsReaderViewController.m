//
//  ProductManualsReaderViewController.m
//  HoneyWell
//
//  Created by Jia.xu on 14-8-4.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import "ProductManualsReaderViewController.h"

@interface ProductManualsReaderViewController ()
<UIWebViewDelegate>
{
    UIWebView *_webView;
}

@end

@implementation ProductManualsReaderViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = NSLocalizedString(TITLE_PRODUCT_MANUALS, nil);
    
    NSString *filepath = nil;
    if (self.mManualType == kManualSetup) {
        self.title = NSLocalizedString(TITLE_INSTALLATION, nil);
        filepath = [[NSBundle mainBundle] pathForResource:@"manual" ofType:@"pdf"];
    }else if (self.mManualType == kManualFixed) {
        filepath = [[NSBundle mainBundle] pathForResource:@"manual" ofType:@"pdf"];
    }
    
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, _heightFromNavBarToBottom)];
    _webView.delegate = self;
    _webView.scalesPageToFit = YES;
    [self.view addSubview:_webView];
    
    [[MMProgressHUD sharedHUD] setPresentationStyle:MMProgressHUDPresentationStyleFade];
    [MMProgressHUD showWithStatus:NSLocalizedString(TITLE_LOADING, nil)];
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:filepath]]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma - mark UIWebViewDelegate
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [MMProgressHUD dismiss];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [MMProgressHUD dismiss];
    NSLog(@"error %@", error);
}

@end
