//
//  ProductManualCollectionViewCell.h
//  HoneyWell
//
//  Created by Jia.xu on 14-8-6.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductManualCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) IBOutlet UILabel *mName;

@end
