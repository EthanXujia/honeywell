//
//  ProductManualsReaderViewController.h
//  HoneyWell
//
//  Created by Jia.xu on 14-8-4.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import "BaseViewController.h"

enum ManualType{
    kManualSetup = 0,
    kManualFixed
};

@interface ProductManualsReaderViewController : BaseViewController

@property (nonatomic, assign) enum ManualType mManualType;


@end
