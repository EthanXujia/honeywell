//
//  CityWeatherCollectionViewCell.h
//  HoneyWell
//
//  Created by Jia.xu on 14-8-5.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CurvilinearView.h"

@interface CurButton : UIButton
@property (nonatomic, assign) CGFloat mDrawHeight;
@end

@interface CityWeatherCollectionViewCell : UICollectionViewCell
<UIScrollViewDelegate>
{
    IBOutlet CurvilinearView *_curlinearView;
    IBOutlet CurButton *_curBtn;
}

@property (nonatomic, strong) IBOutlet UIScrollView *mScrollView;

@property (nonatomic, strong) IBOutlet UILabel *mCityName;
@property (nonatomic, strong) IBOutlet UILabel *mPM2_5;
@property (nonatomic, strong) IBOutlet UILabel *mHumidity;
@property (nonatomic, strong) IBOutlet UILabel *mTemperatureNow;
@property (nonatomic, strong) IBOutlet UILabel *mTemperatureHigh;
@property (nonatomic, strong) IBOutlet UILabel *mTemperatureLow;
@property (nonatomic, strong) IBOutlet UILabel *mWindspeed;
@property (nonatomic, strong) IBOutlet UILabel *mAirQuality;

@property (nonatomic, strong) IBOutlet UILabel *mTips;
@property (nonatomic, strong) IBOutlet UILabel *mClothes;
@property (nonatomic, strong) IBOutlet UILabel *mWashCar;
@property (nonatomic, strong) IBOutlet UILabel *mSunshine;

@property (nonatomic, strong) IBOutlet UILabel *mToday;
@property (nonatomic, strong) IBOutlet UILabel *mTomorrow;
@property (nonatomic, strong) IBOutlet UILabel *mTodayAfterTomorrow;
@property (nonatomic, strong) IBOutlet UILabel *mTodayTemp;
@property (nonatomic, strong) IBOutlet UILabel *mTomorrowTemp;
@property (nonatomic, strong) IBOutlet UILabel *mTodayAfterTomorrowTemp;
@property (nonatomic, strong) IBOutlet UILabel *mTodayWeather;
@property (nonatomic, strong) IBOutlet UILabel *mTomorrowWeather;
@property (nonatomic, strong) IBOutlet UILabel *mTodayAfterTomorrowWeather;

- (IBAction)dragMoving: (UIControl *) c withEvent:ev;
- (IBAction)dragEnded: (UIControl *) c withEvent:ev;

@end
