//
//  HomeViewController.m
//  HoneyWell
//
//  Created by Jia.xu on 14-8-4.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import "HomeViewController.h"
#import "QRScanningViewController.h"
#import "SliderMenuView.h"
#import "CityWeatherCollectionViewCell.h"
#import "NewsViewController.h"
#import "MoreViewController.h"
#import "ProductManualViewController.h"
#import "LocationManager.h"

static NSString *kCell=@"cell";


@interface HomeViewController ()
<UICollectionViewDelegateFlowLayout,
 UICollectionViewDelegate,
 UICollectionViewDataSource,
 UIScrollViewDelegate,
 UITabBarControllerDelegate,
 CLLocationManagerDelegate>
{
    CLLocationManager *_locationManager;    
    UICollectionView *_collectionView;
    NSMutableArray *_cityWeatherList;
    NSInteger _currentIndex;
}

@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    _locationManager.delegate = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = NSLocalizedString(TITLE_AIR, nil);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLocation:) name:NOTIFICATION_UPDATELOCATION object:nil];
    [[LocationManager shareLocationManager] startUpdateLocation];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.minimumLineSpacing = 0;
    flowLayout.minimumInteritemSpacing = 0;
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, _heightFromNavBarToBottom) collectionViewLayout:flowLayout];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.showsHorizontalScrollIndicator = NO;
    _collectionView.pagingEnabled = YES;
    _collectionView.backgroundColor = [UIColor clearColor];
    [_collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([CityWeatherCollectionViewCell class]) bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:kCell];
    [self.view addSubview:_collectionView];
    
}

- (void)updateLocation:(NSNotification*)notification
{
    [_collectionView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchSettingItem:(id)sender
{
    [UIView animateWithDuration:.25f
                          delay:.0f
                        options:UIViewAnimationOptionAllowUserInteraction|UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         _sliderMenuView.frame = _sliderMenuView.hidden ? _sliderMenuTargetFrame : _sliderMenuOriginalFrame;
                         _collectionView.frame = _sliderMenuView.hidden ? CGRectMake(_sliderMenuView.frame.size.width, 0, _collectionView.frame.size.width, _collectionView.frame.size.height) : CGRectMake(0, 0, _collectionView.frame.size.width, _collectionView.frame.size.height);
                         _sliderMenuView.hidden = NO;
                     }
                     completion:^(BOOL finished){
                         if (_sliderMenuView.frame.origin.x < 0) {
                             _sliderMenuView.hidden = YES;
                         }
                     }];
}

#pragma mark - UICollectionViewDataSource
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return _collectionView.frame.size;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsZero;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return TEST_CITY_NUMBERS;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CityWeatherCollectionViewCell* cell =  (CityWeatherCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kCell forIndexPath:indexPath];
    if (indexPath.row != _currentIndex) {
        [cell.mScrollView setContentOffset:CGPointMake(0, 0) animated:NO];
    }
    NSString *cityName = nil;
    if ([Global isOpenGPS]) {
        if (indexPath.row == 0) {
            cityName = [Global getCityName];
        }
    }
    cell.mCityName.text = cityName;
    [cell.mCityName sizeToFit];
    cell.mCityName.center = CGPointMake(cell.center.x, cell.mCityName.center.y);
    cell.mPM2_5.text = [NSString stringWithFormat:@"PM %d", (int)indexPath.row];
    _currentIndex = indexPath.row;
    return cell;
}

@end
