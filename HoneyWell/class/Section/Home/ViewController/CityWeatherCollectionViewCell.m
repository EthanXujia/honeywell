//
//  CityWeatherCollectionViewCell.m
//  HoneyWell
//
//  Created by Jia.xu on 14-8-5.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import "CityWeatherCollectionViewCell.h"

@implementation CurButton

- (void)drawRect:(CGRect)rect
{
    UIColor *bfColor = [UIColor colorWithRed:97.0/255.0 green:173.0/255.0 blue:244.0/255.0 alpha:1.0];
    UIColor *gfColor = [UIColor lightGrayColor];
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(ctx, bfColor.CGColor);
 
    CGPoint point = CGPointMake(self.frame.size.width/2, _mDrawHeight);
    
    // draw line
    CGContextFillEllipseInRect(ctx, CGRectMake(point.x-4, point.y-4, 8, 8));
    CGContextFillPath(ctx);
    
    // draw point
    CGContextSetFillColorWithColor(ctx, gfColor.CGColor);
    CGContextFillRect(ctx, CGRectMake(self.frame.size.width/2, _mDrawHeight+4, 1, self.frame.size.height-_mDrawHeight));

}

@end

@implementation CityWeatherCollectionViewCell

- (void)awakeFromNib
{
    self.clipsToBounds = YES;
    self.mScrollView.contentSize = CGSizeMake(self.frame.size.width, self.mSunshine.frame.origin.y+self.mSunshine.frame.size.height+self.mCityName.frame.origin.y);
    [_curlinearView drawCurvlinear];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (IBAction)dragMoving: (UIControl *) c withEvent:ev
{
    c.center = CGPointMake([[[ev allTouches] anyObject] locationInView:self].x, c.center.y);

//    NSInteger len = _curlinearView.graphView.posArray.count-1;
//    NSInteger index = (NSInteger)(c.center.x/(320/24));
//    index = (index > len ? len : index);
//    CGPoint p = [(NSValue*)(_curlinearView.graphView.posArray[len-index]) CGPointValue];
//    _curBtn.mDrawHeight = p.y;
//    [_curBtn setNeedsDisplay];
    
    for (int i=0; i<60; i++) {
        if ([_curlinearView.graphView.bezierPath containsPoint:CGPointMake(c.center.x, i)]) {
            _curBtn.mDrawHeight = i;//p.y;
            [_curBtn setNeedsDisplay];
            break;
        }
    }
}

- (IBAction)dragEnded: (UIControl *) c withEvent:ev
{
    c.center = CGPointMake([[[ev allTouches] anyObject] locationInView:self].x, c.center.y);
    

}

@end
