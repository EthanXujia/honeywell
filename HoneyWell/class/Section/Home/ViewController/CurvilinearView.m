//
//  CurvilinearView.m
//  HoneyWell
//
//  Created by Jia.xu on 14-8-7.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import "CurvilinearView.h"
#import "Global.h"

@implementation CurvilinearView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        
    }
    return self;
}

- (void)drawCurvlinear
{
    if (!_graphView) {
        _graphView = [[GraphView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [_graphView setBackgroundColor:[UIColor clearColor]];
        [_graphView setSpacing:320/24];
        [_graphView setFill:NO];
        [_graphView setStrokeColor:[UIColor lightGrayColor]];
        [_graphView setZeroLineStrokeColor:[UIColor greenColor]];
        [_graphView setFillColor:[UIColor orangeColor]];
        [_graphView setLineWidth:1];
        [_graphView setCurvedLines:YES];
        [self addSubview:_graphView];

        NSArray *array = @[@100.0f,
                           @101.0f,
                           @102.0f,
                           @103.0f,
                           @214.0f,
                           @313.0f,
                           @512.0f,
                           @101.0f,
                           @10.0f,
                           @90.0f,
                           @80.0f,
                           @07.0f,
                           @06.0f,
                           @50.0f,
                           @10.0f,
                           @53.0f,
                           @26.0f,
                           @100.0f,
                           @520.0f,
                           @49.0f,
                           @423.0f,
                           @428.0f,
                           @1500.0f,
                           @60.0f];

        [_graphView setArray:array];
        
    }
    
}

- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
}

@end
