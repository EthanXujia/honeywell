


#import <Foundation/Foundation.h>

enum MyAPIErrorType {
	ET_ConnectionError,
	ET_AuthError,
	ET_UnknownError
};

@interface MyAPIException : NSException {
    NSString* errorMessage;
    //enum MyAPIErrorType errorType;
    int errorType;
}

@property (nonatomic, retain) NSString* errorMessage;
@property (nonatomic, assign) int errorType;
//@property (nonatomic, assign) enum MyAPIErrorType errorType;

//- (id) initWithErrorMessage : (NSString*) message errorCode : (enum MyAPIErrorType) errorCode;
- (id) initWithErrorMessage : (NSString*) message errorCode : (int) errorCode;

@end
