//
//  AsyncCallParameters.m
//  mihui
//
//  Created by Yifan Wang on 4/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AsyncCallParameters.h"

@implementation AsyncCallParameters
@synthesize mTag;
@synthesize mParameter1;
@synthesize mParameter2;
@synthesize mParameter3;
@synthesize mParameter4;
@synthesize mParameter5;

- (void) dealloc {
    mTag = nil;
    mParameter1 = nil;
    mParameter2 = nil;
    mParameter3 = nil;
    mParameter4 = nil;
    mParameter5 = nil;
}


@end