//
//  MyHttpProtocol.h
//  qcntv
//
//  Created by jia xu on 12-6-15.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AsyncCallResult.h"

@protocol MyHttpAPIDelegate <NSObject>
@optional

- (void)getRequestFinished:(AsyncCallResult*)resultInfo;


@end


@protocol MyHttpProtocol <NSObject>
- (void)setDelegate:(id<MyHttpAPIDelegate>) delegate;

- (BOOL)startRequest:(NSString*)url withTag:(int)tag withBodyData:(NSData*)data withPostMethod:(BOOL)isPost;

//- (void)startGetInfo:(NSString*)jsonString withTag:(NSString*)tag withMethodName:(NSString*)methodName withAction:(NSString*)action;
//
//// load data from http API. If isMock is YES, it's testing data
//- (void)startGetInfo:(NSString*)jsonString withTag:(NSString*)tag withMethodName:(NSString*)methodName withAction:(NSString*)action withMock:(BOOL)isMock;
//
//- (void)startGetVersion;
//
//- (void)startGetEvents;

@end
