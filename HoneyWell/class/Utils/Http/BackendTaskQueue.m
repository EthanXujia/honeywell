//
//  BackendTaskQueue.m
//  Player
//
//  Created by Yifan Wang on 10/1/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "BackendTaskQueue.h"

static BackendTaskQueue* backEndTaskQueueInstance;

@implementation BackendTaskQueue


+ (BackendTaskQueue*) instance {
	if(backEndTaskQueueInstance == nil) {
		backEndTaskQueueInstance = [[BackendTaskQueue alloc] init];
	}
	return backEndTaskQueueInstance;
}

- (void) dealloc {
//    [queue release];
//    [super dealloc];
}

- (id) init {
    if ((self = [super init])) {
	    queue = [NSOperationQueue new];
		[queue setMaxConcurrentOperationCount:1];
        
	}
	return self;
}

- (void)cancelAllOperations
{
    if (queue) {
        [queue cancelAllOperations];
    }
}

- (void)addOperation:(id)target selector:(SEL)sel object:(id)arg {
	NSInvocationOperation *operation = [[NSInvocationOperation alloc]
										 initWithTarget:target
										 selector:sel
										 object:arg];

	[queue addOperation:operation];
//	[operation release];
}



@end
