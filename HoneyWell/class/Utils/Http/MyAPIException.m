


#import "MyAPIException.h"


@implementation MyAPIException
@synthesize errorMessage;
@synthesize errorType;

- (id) initWithErrorMessage : (NSString*) message errorCode : (int) errorCode {
	if ((self = [super initWithName:@"myAPIException" reason:message userInfo:nil])) {
		self.errorType = errorCode;
		self.errorMessage = message;
	}
	return self;    
}

-(void)dealloc {

    
}


@end
