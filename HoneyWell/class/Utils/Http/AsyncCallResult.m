//
//  AsyncCallResult.m
//  mihui
//
//  Created by Ethan Xu on 4/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AsyncCallResult.h"


@implementation AsyncCallResult
@synthesize mTag;
@synthesize mException;
@synthesize mValue;
@synthesize mValueHeader;
@synthesize mValueInfo;

- (void) dealloc {
    mTag = nil;
    mException = nil;
    mValue = nil;
    mValueHeader = nil;
    mValueInfo = nil;
}

-(BOOL) isSuccess {
    return mException == nil;
}

@end
