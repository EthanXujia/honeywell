//
//  BackendTaskQueue.h
//  Player
//
//  Created by Yifan Wang on 10/1/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BackendTaskQueue : NSObject {
	NSOperationQueue* queue; //used for backend loading
}

+ (BackendTaskQueue*) instance;

- (id) init;
- (void)addOperation:(id)target selector:(SEL)sel object:(id)arg;
- (void)cancelAllOperations;

@end
