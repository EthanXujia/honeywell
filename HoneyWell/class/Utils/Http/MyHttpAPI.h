//
//  MyHttpAPI.h
//  reader
//
//  Created by iXujia xu on 11-12-15.
//  Copyright (c) 2011年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"
#import "MyHttpProtocol.h"


@interface MyHttpAPI : NSObject<MyHttpProtocol, ASIHTTPRequestDelegate> {
    __weak NSObject<MyHttpAPIDelegate> *mDelegate;
    
@private
    NSMutableData *_data;
}

@property (nonatomic, weak) NSObject<MyHttpAPIDelegate> *mDelegate;


- (id) accessWebAPI:(NSString*) httpString httpMethod:(NSString*) method body:(NSData*) data usingMultipart:(BOOL)usingMultipart withTag:(int)tag;
+ (NSString*)getDomain;
+ (id<MyHttpProtocol>) createMyHttpAPI;
- (void)setDelegate:(id<MyHttpAPIDelegate>) delegate;

@end
