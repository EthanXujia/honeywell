//
//  AsyncCallParameters.h
//  mihui
//
//  Created by Yifan Wang on 4/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AsyncCallParameters : NSObject {
    id mTag;
    id mParameter1;
    id mParameter2;
    id mParameter3;
    id mParameter4;
    id mParameter5;
}

@property (nonatomic, retain) id mTag;
@property (nonatomic, retain) id mParameter1;
@property (nonatomic, retain) id mParameter2;
@property (nonatomic, retain) id mParameter3;
@property (nonatomic, retain) id mParameter4;
@property (nonatomic, retain) id mParameter5;

@end