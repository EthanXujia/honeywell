//
//  AsyncCallResult.h
//  mihui
//
//  Created by Ethan Xu on 4/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyAPIException.h"

@interface AsyncCallResult : NSObject {
    id mTag;
    MyAPIException* mException;
    id mValue;
    id mValueHeader;
    id mValueInfo;
}

@property (nonatomic, retain) id mTag;
@property (nonatomic, retain) MyAPIException* mException;
@property (nonatomic, retain) id mValue;
@property (nonatomic, retain) id mValueHeader;
@property (nonatomic, retain) id mValueInfo;

-(BOOL) isSuccess;

@end
