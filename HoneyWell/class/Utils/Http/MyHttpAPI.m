//
//  MyHttpAPI.m
//  reader
//
//  Created by iXujia xu on 11-12-15.
//  Copyright (c) 2011年 __MyCompanyName__. All rights reserved.
//

#import "MyHttpAPI.h"
#import "JSON.h"
#import "Global.h"
#import "AsyncCallResult.h"
#import "MyAPIException.h"
#import "ASINSStringAdditions.h"
#import "AsyncCallParameters.h"
#import "BackendTaskQueue.h"
#import "NSString+URLEncode.h"
#import "ASIFormDataRequest.h"


#define USE_MOCKAPI NO
#define USE_SOAP NO

@implementation MyHttpAPI 
@synthesize mDelegate;

static NSString* domain = @"";

static const NSString* gMultipartBoundary = @"0xKhTmLbOuNdArY";

+ (NSString*)getDomain
{
    return domain;
}

+ (id<MyHttpProtocol>) createMyHttpAPI {
    return [[MyHttpAPI alloc] init];
}

- (void)setDelegate:(id<MyHttpAPIDelegate>) delegate {
    mDelegate = delegate;
}

- (id) accessWebAPI:(NSString*) httpString httpMethod:(NSString*) method body:(NSData*) data usingMultipart:(BOOL)usingMultipart withTag:(int)tag
{    
    //this is just for testing
    //[NSThread sleepForTimeInterval:2.0f];
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:httpString]];
    request.tag = tag;
    [request setRequestMethod:method];
    //[request setClientCertificateIdentity:[Global getSecIdentity]];
    //[request setValidatesSecureCertificate:YES];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    [dateFormatter setDateFormat:@"ZZZZ"];
    NSString *strGMT = [dateFormatter stringFromDate:[NSDate date]];
    
    [request addRequestHeader:@"X-GMT" value:strGMT];
    [request addRequestHeader:@"X-TimeZone" value:[[NSTimeZone localTimeZone] name]];
    [request addRequestHeader:@"X-Language" value:[[NSLocale preferredLanguages] objectAtIndex:0]];
    if (data) {
        // Notice: this request header and value field must be add
        [request addRequestHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
        [request addRequestHeader:@"Content-Length" value:[NSString stringWithFormat:@"%u", [data length]]];
        [request appendPostData:data];
    }
    [request setTimeOutSeconds:60];
    [request startSynchronous];
    
    NSError *error = [request error];
    if (error) {// network error
        AsyncCallResult *result = [[AsyncCallResult alloc] init];
        result.mException = [[MyAPIException alloc] initWithErrorMessage:[request.error description] errorCode:0];
        [mDelegate performSelectorOnMainThread:@selector(getRequestFinished:)
                                    withObject:result waitUntilDone:NO];
        return nil;
    }
    
    NSString *string = [[NSString alloc] initWithData:[request responseData] encoding:[request responseEncoding]];
    //NSLog(@"string is <%@>", string);
    id obj = [string JSONValue];
    AsyncCallResult *result = [[AsyncCallResult alloc] init];
    result.mTag = request;
    result.mValue = obj;

    if (mDelegate) {
        [mDelegate performSelectorOnMainThread:@selector(getRequestFinished:)
                                    withObject:result waitUntilDone:NO];
    }else{
        
    }
    
    return nil;
}

- (BOOL)startRequestSecond:(NSString*)url withTag:(int)tag withBodyData:(NSData*)data withPostMethod:(BOOL)isPost
{
    
    AsyncCallParameters *parameters = [[AsyncCallParameters alloc] init];
    parameters.mParameter1 = url;
    parameters.mParameter2 = data;
    parameters.mParameter3 = [NSNumber numberWithInt:tag];
    parameters.mParameter4 = [NSNumber numberWithBool:isPost];
    
    [[BackendTaskQueue instance] addOperation:self selector:@selector(getHttpsInternal:) object:parameters];
    return YES;
}

- (BOOL)startRequest:(NSString*)url withTag:(int)tag withBodyData:(NSData*)data withPostMethod:(BOOL)isPost
{
    AsyncCallParameters *parameters = [[AsyncCallParameters alloc] init];
    parameters.mParameter1 = url;
    parameters.mParameter2 = data;
    parameters.mParameter3 = [NSNumber numberWithInt:tag];
    parameters.mParameter4 = [NSNumber numberWithBool:isPost];

    [[BackendTaskQueue instance] addOperation:self selector:@selector(getHttpsInternal:) object:parameters];
    return YES;
}

- (void)getHttpsInternal:(AsyncCallParameters*)parameters
{
    NSString *url = parameters.mParameter1;
    NSData *data = parameters.mParameter2;
    int tag = [parameters.mParameter3 intValue];
    BOOL isPost = [parameters.mParameter4 boolValue];
    [self accessWebAPI:url httpMethod:(isPost?@"POST":@"GET") body:data usingMultipart:NO withTag:tag];
}

#pragma -mark ASIHttpRequestDelegate
- (void)requestStarted:(ASIHTTPRequest *)request
{
    //NSLog(@"request started");
    
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    //NSLog(@"request finished");
    NSString *string = [[NSString alloc] initWithData:_data encoding:[request responseEncoding]];
    id obj = [string JSONValue];
    AsyncCallResult *result = [[AsyncCallResult alloc] init];
    result.mTag = request;
    result.mValue = obj;
    [mDelegate performSelectorOnMainThread:@selector(getRequestFinished:)
                                withObject:result waitUntilDone:NO];

}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    AsyncCallResult *result = [[AsyncCallResult alloc] init];
    result.mException = [[MyAPIException alloc] initWithErrorMessage:[request.error description] errorCode:0];
    [mDelegate performSelectorOnMainThread:@selector(getRequestFinished:)
                                withObject:result waitUntilDone:NO];
}

- (void)request:(ASIHTTPRequest *)request didReceiveData:(NSData *)data
{
    if (!_data) {
        _data = [[NSMutableData alloc] init];
    }
    [_data appendData:data];
}

/*
- (AsyncCallResult*) upLoadQQInternal:(AsyncCallParameters*) parameters {
        
    MyAPIException* exception = nil;
    id dic = nil;
    
    @try {
        
        NSMutableData* multiPartBody = [NSMutableData dataWithCapacity:[picData length] + 1024];		

        [multiPartBody appendData:[[NSString stringWithFormat:@"--%@\r\n",gMultipartBoundary] dataUsingEncoding:NSUTF8StringEncoding]];		
        [multiPartBody appendData:[@"Content-Disposition: form-data; name=\"picture\"; filename=\"pic.jpeg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [multiPartBody appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [multiPartBody appendData:picData];

        [multiPartBody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",gMultipartBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [multiPartBody appendData:[@"Content-Disposition: form-data; name=\"parameter\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

        [multiPartBody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",gMultipartBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString* str = [NSString stringWithFormat:@"format=json&content=%@&clientip=127.0.0.1&jing=&wei=&pic=%@&syncflag=0", @"我testing", ];
        
        id data = [self accessWebAPI: 
                          httpMethod: @"POST"
                                body: nil
                            usingMultipart: YES];
		dic = data;
        //NSLog(@"dic = %@", dic);
        if ([dic isKindOfClass:[NSDictionary class]]) {
            if ([dic objectForKey:@"ret"]) {
                int ret = [[dic objectForKey:@"ret"] intValue];
                if (ret != 0) {
                    exception = [[[MyAPIException alloc] initWithErrorMessage:[dic objectForKey:@"msg"] errorCode:[[dic objectForKey:@"ret"] intValue]] autorelease];                    
                }
            }            
        }
    }
    @catch (MyAPIException* e) {
        exception = e;
    }
    @catch (NSException* e) {
        exception = [[[MyAPIException alloc] initWithErrorMessage:@"System Error" errorCode:0] autorelease];
    }
    @finally {		
        
    }
    
    AsyncCallResult* result = [[[AsyncCallResult alloc] init] autorelease];
	result.mException = exception;
    result.mValue = dic;
    [mDelegate performSelectorOnMainThread:@selector(getShareQQFinished:) 
                                withObject:result waitUntilDone:NO];
    return result;
    
}
*/



@end
