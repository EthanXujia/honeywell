//
//  PhotoProcessingHelper.m
//  mihui
//
//  Created by xujia on 4/4/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PhotoProcessingHelper.h"
#import "UIImage+Resize.h"


@implementation PhotoProcessingHelper

//------- xujia copy ------------------------------------------------------------------
+(UIImage*) drawImage:(UIImage*)image size:(CGSize)size rect:(CGRect)rect
{
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	CGContextRef context ;
	context= CGBitmapContextCreate (NULL, size.width,size.height,  8, 0, colorSpace, kCGImageAlphaPremultipliedFirst);
	
	UIImageOrientation orientation = image.imageOrientation;
	switch (orientation) {
		case UIImageOrientationDown:
			CGContextTranslateCTM(context, size.width, size.height);
			CGContextRotateCTM(context, M_PI);
			CGContextDrawImage(context, rect, image.CGImage);
			break;
		case UIImageOrientationRight:
			CGContextTranslateCTM(context, 0, size.height);
			CGContextRotateCTM(context, 3.0*M_PI / 2.0);
			CGContextDrawImage(context, CGRectMake(rect.origin.x, rect.origin.y, rect.size.height, rect.size.width), image.CGImage);
			break;
		case UIImageOrientationLeft:
			CGContextTranslateCTM(context, size.width, 0);
			CGContextRotateCTM(context, M_PI / 2.0);
			CGContextDrawImage(context, CGRectMake(rect.origin.x, rect.origin.y, rect.size.height, rect.size.width), image.CGImage);
			break;
		default:
			CGContextDrawImage(context, rect, image.CGImage);
			break;
	}
	
	CGImageRef retRef = CGBitmapContextCreateImage(context);
	CGContextRelease(context);
	CGColorSpaceRelease(colorSpace);
	UIImage *retImg = [UIImage imageWithCGImage:retRef];
	CGImageRelease(retRef);
	return retImg;
}

+(CGRect) getIntersectForImage:(UIImage*)image andCropRect:(CGRect)cropRect
{
	CGRect rect = CGRectMake(fmaxf(cropRect.origin.x, 0.0),
							 fmaxf(cropRect.origin.y, 0.0), 0, 0);
	
	if (cropRect.origin.x >= 0) {
		rect.size.width = fminf(cropRect.size.width, image.size.width - abs(cropRect.origin.x));
	} else {
		rect.size.width = fminf(image.size.width, cropRect.size.width  - abs(cropRect.origin.x));
	}
	if (cropRect.origin.y >= 0) {
		rect.size.height = fminf(cropRect.size.height, image.size.height - abs(cropRect.origin.y));
	} else {
		rect.size.height = fminf(image.size.height, cropRect.size.height  - abs(cropRect.origin.y));
	}
	
	return rect;
}
+(CGRect) getDrawRectForDrawSize:(CGSize)drawSize imageSize:(CGSize)imageSize
{	
	CGFloat x, y, w, h;
	CGFloat	ratioImage = imageSize.width / imageSize.height;
	CGFloat ratioDraw = drawSize.width / drawSize.height;
	
	if (ratioImage == ratioDraw) {
		return CGRectMake(0.0, 0.0, drawSize.width, drawSize.height);
	} else if (ratioImage < ratioDraw) {
		x = 0.0;
		w = drawSize.width;
		h = drawSize.width / ratioImage;
		y = (drawSize.height - h) / 2;
	} else {
		y = 0.0;
		h = drawSize.height;
		w = drawSize.height * ratioImage;
		x = (drawSize.width - w) / 2;
	}
	
	return CGRectMake(x, y, w, h);
}

+(UIImage*) cropImage:(UIImage*)image to:(CGRect)cropRect andScaleTo:(CGSize)size
{
	if (image.imageOrientation != UIImageOrientationUp) {
		image = [self drawImage:image size:image.size rect:CGRectMake(0, 0, image.size.width, image.size.height)];
	}
    
	UIGraphicsBeginImageContext(size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	[[UIColor blackColor] setFill];
	UIRectFill(CGRectMake(0.0, 0.0, size.width, size.height));
	
    CGImageRef subImage = CGImageCreateWithImageInRect([image CGImage], cropRect);
    
	CGRect rectImage = [self getIntersectForImage:image andCropRect:cropRect];
	CGRect drawRect = [self getDrawRectForDrawSize:size imageSize:rectImage.size];
    
	CGContextScaleCTM(context, 1.0f, -1.0f);
    CGContextTranslateCTM(context, 0.0f, -size.height);
	CGContextDrawImage(context, drawRect, subImage);
    UIImage* croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    CGImageRelease(subImage);
	UIGraphicsEndImageContext();
	
    return croppedImage;
}
//----------------------------------------------------------------------------------------

+ (NSData*) processPhoto:(UIImage *)originalImage clipRect:(CGRect)clipRect scaleWidth:(double)squareWidth {
    UIImage* image = [originalImage getSubImageFrom:clipRect];
    image = [image resizedImage:CGSizeMake(squareWidth, squareWidth) interpolationQuality:kCGInterpolationHigh];
    return UIImageJPEGRepresentation(image, 0.715);
}


+ (NSData*) processPhoto : (UIImage*) originalImage 
                clipRect : (CGRect) clipRect 
             squareWidth : (double) squareWidth {
    UIImage* image = [originalImage getSubImageFrom:clipRect];
    
    int newSize = image.size.width > squareWidth ? squareWidth : image.size.width;      
    image = [image resizedImage:CGSizeMake(newSize, newSize) interpolationQuality:kCGInterpolationHigh];
        
    return UIImageJPEGRepresentation(image, 0.715);
}

+ (UIImage *)addImageview:(UIImage *)image toSize:(CGSize) targetSize {
    CGSize size= CGSizeMake(targetSize.width, targetSize.height);
    UIGraphicsBeginImageContext(size);
    // Draw image1
    //    [image2 drawInRect:CGRectMake(0, 0, image2.size.width, image2.size.height)];
    // Draw image2    
    [image drawInRect:CGRectMake((targetSize.width-image.size.width)/2, (targetSize.width-image.size.height)/2, image.size.width, image.size.height)];
    UIImage *resultingImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resultingImage;
}


+ (UIImage*)scaleImage:(UIImage*)image to:(CGSize)size
{
    if (image.size.width <= size.width && image.size.height <= size.height) {
        return image;
    }
    CGFloat w = image.size.width/size.width;
    CGFloat h = image.size.height/size.height;
    CGFloat scale = (w > h ? w : h);
    NSData *data = UIImageJPEGRepresentation(image, .5f);
    return [UIImage imageWithData:data scale:scale];
}

+ (NSData*)scaleImageData:(NSData*)data to:(CGSize)size
{
    if (!data) {
        return nil;
    }
    UIImage *image = [UIImage imageWithData:data];
    CGFloat w = image.size.width/size.width;
    CGFloat h = image.size.height/size.height;
    if (w < 1 && h < 1) {
        return data;
    }
    image = nil;
    CGFloat scale = (w > h ? w : h);
    UIImage *img = [UIImage imageWithData:data scale:scale];
    return UIImageJPEGRepresentation(img, .5f);
}

@end
