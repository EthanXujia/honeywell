//
//  UIImage+FromColor.m
//  Pretect
//
//  Created by Jia.xu on 3/11/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import "UIImage+FromColor.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIImage (FromColor)


+ (UIImage *) buttonImageFromColor:(UIColor *)color withSize:(CGSize)size
{
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}


@end
