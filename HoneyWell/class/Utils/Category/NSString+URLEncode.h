//
//  NSString+URLEncode.h
//  Pretect
//
//  Created by Jia.xu on 14-3-14.
//  Copyright (c) 2014年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (URLEncode)

- (NSString *)URLEncodedString;

@end
