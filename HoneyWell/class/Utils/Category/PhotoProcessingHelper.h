//
//  PhotoProcessingHelper.h
//  mihui
//
//  Created by xujia on 4/4/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PhotoProcessingHelper : NSObject {
    
}

+(UIImage*) drawImage:(UIImage*)image size:(CGSize)size rect:(CGRect)rect;
+(UIImage*) cropImage:(UIImage*)image to:(CGRect)cropRect andScaleTo:(CGSize)size;
+ (NSData*) processPhoto : (UIImage*) originalImage clipRect : (CGRect) clipRect squareWidth : (double) squareWidth;
+ (UIImage *)addImageview:(UIImage *)image toSize:(CGSize) targetSize;

// if the image's size is bigger than the size then scaling the image or return the orignal image
+ (UIImage*)scaleImage:(UIImage*)image to:(CGSize)size;
+ (NSData*)scaleImageData:(NSData*)data to:(CGSize)size;


@end
