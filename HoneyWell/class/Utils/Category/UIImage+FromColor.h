//
//  UIImage+FromColor.h
//  Pretect
//
//  Created by Jia.xu on 3/11/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (FromColor)

+ (UIImage *) buttonImageFromColor:(UIColor *)color withSize:(CGSize)size;

@end
