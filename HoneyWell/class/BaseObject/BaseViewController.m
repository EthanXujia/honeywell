//
//  BaseViewController.m
//  CsetMail
//
//  Created by Jia.xu on 14-7-15.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import "BaseViewController.h"
#import "HomeViewController.h"
#import "NewsViewController.h"
#import "ProductManualViewController.h"
#import "EditLocationViewController.h"
#import "SettingViewController.h"
#import "FeedbackViewController.h"
#import "QRScanningViewController.h"
#import "ProductShoppingViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController
@synthesize mAPI=_mAPI;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    if (_sliderMenuView) {
        _sliderMenuView.mDelegate = nil;
    }
    [_mAPI setDelegate:nil];
    _mAPI = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([self isKindOfClass:[HomeViewController class]] || [self isKindOfClass:[NewsViewController class]] || [self isKindOfClass:[ProductManualViewController class]]) {
        TabMenuView *_tab = [Global getTabMenuView];
        _tab.frame = CGRectMake(0, _tabPosY, _tab.frame.size.width, _tab.frame.size.height);
        [self.view addSubview:_tab];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    _mAPI = [MyHttpAPI createMyHttpAPI];
    [_mAPI setDelegate:self];
    [self initBackButtonItem];
    
    _startY = (IOS7_OR_LATER ? 20 : 0)+self.navigationController.navigationBar.frame.size.height;
    _tabPosY = self.view.frame.size.height-self.navigationController.navigationBar.frame.size.height-(IOS7_OR_LATER ? 20 : 0)-[Global getTabMenuView].frame.size.height;
    _heightFromNavBarToBottom = self.view.frame.size.height-_startY;
    
    if ([self isKindOfClass:[HomeViewController class]] || [self isKindOfClass:[NewsViewController class]] || [self isKindOfClass:[ProductManualViewController class]]) {
        _sliderMenuOriginalFrame = CGRectMake(-self.view.frame.size.width/2, 0, self.view.frame.size.width/2, self.view.frame.size.height-self.navigationController.navigationBar.frame.size.height-(IOS7_OR_LATER?20:0)-self.tabBarController.tabBar.frame.size.height);
        _sliderMenuTargetFrame = CGRectMake(0, 0, _sliderMenuOriginalFrame.size.width, _sliderMenuOriginalFrame.size.height);
        _sliderMenuView = [[SliderMenuView alloc] initSliderMenuViewWithFrame:_sliderMenuOriginalFrame withItems:@[NSStringFromClass([EditLocationViewController class]), NSStringFromClass([SettingViewController class]), NSStringFromClass([FeedbackViewController class])] withDelegate:self];
        _sliderMenuView.hidden = YES;
        [self.view addSubview:_sliderMenuView];
        
        UIBarButtonItem *settingBtnItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemOrganize target:self action:@selector(touchSettingItem:)];
        UIBarButtonItem *rightBtnItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:self action:@selector(touchRightItem:)];
        self.navigationItem.leftBarButtonItem = settingBtnItem;
        self.navigationItem.rightBarButtonItem = rightBtnItem;
        _heightFromNavBarToBottom -= [Global getTabMenuView].frame.size.height;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initBackButtonItem
{
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(touchBack:)];
    self.navigationItem.backBarButtonItem = backItem;
}

- (void)touchBack:(id)sender
{
    if (self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)touchSettingItem:(id)item
{
    
}

- (void)touchRightItem:(id)sender
{
    BaseViewController *vc = nil;
    if (TEST_ISSCANNED) {
        vc = [[ProductShoppingViewController alloc] init];
    }else{
        vc = [[QRScanningViewController alloc] init];
    }    
    [[Global getNavigationController] pushViewController:vc animated:YES];
}


#pragma mark - SliderMenuViewDelegate
- (void)sliderMenuView:(SliderMenuView*)sliderMenuView withSelectIndex:(NSUInteger)index withClassName:(NSString*)className
{
    BaseViewController *vc = [[NSClassFromString(className) alloc] init];
    NSAssert(vc, @"the viewController named \"%@\" is nil, please check your className", className);
    if (vc) {
        [self.navigationController pushViewController:vc animated:YES];
    }
}

@end
