//
//  BaseViewController.h
//  CsetMail
//
//  Created by Jia.xu on 14-7-15.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyHttpAPI.h"
#import "Global.h"
#import "SliderMenuView.h"

@interface BaseViewController : UIViewController
<MyHttpAPIDelegate>
{
    SliderMenuView *_sliderMenuView;
    CGRect _sliderMenuOriginalFrame;
    CGRect _sliderMenuTargetFrame;
    
    CGFloat _startY;
    CGFloat _tabPosY;
    CGFloat _heightFromNavBarToBottom;
}

@property (nonatomic, strong) id<MyHttpProtocol> mAPI;


@end
