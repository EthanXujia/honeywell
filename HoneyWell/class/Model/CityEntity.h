//
//  CityEntity.h
//  HoneyWell
//
//  Created by Jia.xu on 14-8-5.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CityEntity : NSManagedObject

@property (nonatomic, retain) NSString * cityName;
@property (nonatomic, retain) NSString * cityCode;

@end
