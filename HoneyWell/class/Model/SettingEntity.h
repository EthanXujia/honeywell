//
//  SettingEntity.h
//  HoneyWell
//
//  Created by Jia.xu on 14-8-5.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class InfoEntity, ManualEntity, ShoppingEntity;

@interface SettingEntity : NSManagedObject

@property (nonatomic, retain) NSNumber * openGPS;
@property (nonatomic, retain) NSString * productModel;
@property (nonatomic, retain) NSString * qrinfo;
@property (nonatomic, retain) NSNumber * temperature;
@property (nonatomic, retain) NSNumber * windspeed;
@property (nonatomic, retain) NSSet *inInfoEntity;
@property (nonatomic, retain) NSSet *inManualEntity;
@property (nonatomic, retain) NSSet *inShoppingEntity;
@end

@interface SettingEntity (CoreDataGeneratedAccessors)

- (void)addInInfoEntityObject:(InfoEntity *)value;
- (void)removeInInfoEntityObject:(InfoEntity *)value;
- (void)addInInfoEntity:(NSSet *)values;
- (void)removeInInfoEntity:(NSSet *)values;

- (void)addInManualEntityObject:(ManualEntity *)value;
- (void)removeInManualEntityObject:(ManualEntity *)value;
- (void)addInManualEntity:(NSSet *)values;
- (void)removeInManualEntity:(NSSet *)values;

- (void)addInShoppingEntityObject:(ShoppingEntity *)value;
- (void)removeInShoppingEntityObject:(ShoppingEntity *)value;
- (void)addInShoppingEntity:(NSSet *)values;
- (void)removeInShoppingEntity:(NSSet *)values;

@end
