//
//  ManualEntity.h
//  HoneyWell
//
//  Created by Jia.xu on 14-8-5.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SettingEntity;

@interface ManualEntity : NSManagedObject

@property (nonatomic, retain) NSString * manualName;
@property (nonatomic, retain) NSString * manualUrl;
@property (nonatomic, retain) NSString * productModel;
@property (nonatomic, retain) SettingEntity *inSettingEntity;

@end
