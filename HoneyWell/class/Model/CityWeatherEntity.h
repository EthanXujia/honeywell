//
//  CityWeatherEntity.h
//  HoneyWell
//
//  Created by Jia.xu on 14-8-7.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CityWeatherEntity : NSManagedObject

@property (nonatomic, retain) NSString * cityName;
@property (nonatomic, retain) NSString * cityCode;

@end
