//
//  InfoEntity.m
//  HoneyWell
//
//  Created by Jia.xu on 14-8-5.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import "InfoEntity.h"
#import "SettingEntity.h"


@implementation InfoEntity

@dynamic newsId;
@dynamic newsUrl;
@dynamic productModel;
@dynamic inSettingEntity;

@end
