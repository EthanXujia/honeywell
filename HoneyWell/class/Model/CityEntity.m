//
//  CityEntity.m
//  HoneyWell
//
//  Created by Jia.xu on 14-8-5.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import "CityEntity.h"


@implementation CityEntity

@dynamic cityName;
@dynamic cityCode;

@end
