//
//  ShoppingEntity.m
//  HoneyWell
//
//  Created by Jia.xu on 14-8-5.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import "ShoppingEntity.h"
#import "SettingEntity.h"


@implementation ShoppingEntity

@dynamic productId;
@dynamic productImageUrl;
@dynamic productName;
@dynamic productShopUrl;
@dynamic inSettingEntity;

@end
