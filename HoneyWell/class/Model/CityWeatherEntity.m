//
//  CityWeatherEntity.m
//  HoneyWell
//
//  Created by Jia.xu on 14-8-7.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import "CityWeatherEntity.h"


@implementation CityWeatherEntity

@dynamic cityName;
@dynamic cityCode;

@end
