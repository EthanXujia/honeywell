//
//  InfoEntity.h
//  HoneyWell
//
//  Created by Jia.xu on 14-8-5.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SettingEntity;

@interface InfoEntity : NSManagedObject

@property (nonatomic, retain) NSString * newsId;
@property (nonatomic, retain) NSString * newsUrl;
@property (nonatomic, retain) NSString * productModel;
@property (nonatomic, retain) SettingEntity *inSettingEntity;

@end
