//
//  ManualEntity.m
//  HoneyWell
//
//  Created by Jia.xu on 14-8-5.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import "ManualEntity.h"
#import "SettingEntity.h"


@implementation ManualEntity

@dynamic manualName;
@dynamic manualUrl;
@dynamic productModel;
@dynamic inSettingEntity;

@end
