//
//  SettingEntity.m
//  HoneyWell
//
//  Created by Jia.xu on 14-8-5.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import "SettingEntity.h"
#import "InfoEntity.h"
#import "ManualEntity.h"
#import "ShoppingEntity.h"


@implementation SettingEntity

@dynamic openGPS;
@dynamic productModel;
@dynamic qrinfo;
@dynamic temperature;
@dynamic windspeed;
@dynamic inInfoEntity;
@dynamic inManualEntity;
@dynamic inShoppingEntity;

@end
