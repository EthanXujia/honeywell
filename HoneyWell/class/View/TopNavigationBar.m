//
//  TopNavigationBar.m
//  HoneyWell
//
//  Created by Jia.xu on 14-8-5.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import "TopNavigationBar.h"
#import "Global.h"

#define IOS7_TOPBAR CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)
#define IOS6_TOPBAR CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44)

@implementation TopNavigationBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (instancetype)initTopNavigationBar
{
    CGRect frame = CGRectZero;
    frame = IOS7_OR_LATER ? IOS7_TOPBAR : IOS6_TOPBAR;
    if (self = [super initWithFrame:frame]) {
        _leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _leftBtn.hidden = YES;
        _leftBtn.backgroundColor = [UIColor clearColor];
        [_leftBtn addTarget:self action:@selector(touchLeftBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_leftBtn];
        _rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _rightBtn.hidden = YES;
        _rightBtn.backgroundColor = [UIColor clearColor];
        [_rightBtn addTarget:self action:@selector(touchRightBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_rightBtn];
        _titleLabel = [[UILabel alloc] initWithFrame:frame];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.hidden = YES;
        [self addSubview:_titleLabel];
    }
    return self;
}

@end
