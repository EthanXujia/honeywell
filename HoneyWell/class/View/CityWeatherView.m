//
//  CityWeatherView.m
//  HoneyWell
//
//  Created by Jia.xu on 14-8-4.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import "CityWeatherView.h"
#import "Global.h"

@implementation CityWeatherView

- (instancetype)initWithFrame:(CGRect)frame withWeatherInfo:(id)info
{
    if (self = [super initWithFrame:frame]) {
        
        // just have a colorful test
        self.layer.masksToBounds = YES;
        self.layer.borderColor = [UIColor orangeColor].CGColor;
        self.layer.borderWidth = 2.0f;
        
        _mCityLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 50, frame.size.width, 100)];
        _mCityLabel.backgroundColor = [UIColor clearColor];
        _mCityLabel.textColor = [UIColor redColor];
        _mCityLabel.textAlignment = NSTextAlignmentCenter;
        _mCityLabel.font = [UIFont boldSystemFontOfSize:20];
        [self addSubview:_mCityLabel];
        
    }
    return self;
}

@end
