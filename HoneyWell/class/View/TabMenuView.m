//
//  TabMenuView.m
//  HoneyWell
//
//  Created by Jia.xu on 14-8-5.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import "TabMenuView.h"
#import "Global.h"

#define SYSTEM_TABBAR   1
#define TAB_MENU_VIEW_HEIGHT 49

@implementation TabMenuView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (instancetype)initTabMenuView
{
    if (SYSTEM_TABBAR) {
        UITabBar *_tabBar = [[UITabBar alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, TAB_MENU_VIEW_HEIGHT)];
        if (self = [super initWithFrame:_tabBar.frame]){
            _tabBar.delegate = self;
            _tabBar.items = [NSArray arrayWithObjects:
                             [self createItemWithImage:@"" withTitle:@"天气空气" withTag:0],
                             [self createItemWithImage:@"" withTitle:@"最新消息" withTag:1],
                             [self createItemWithImage:@"" withTitle:@"产品手册" withTag:2], nil];
            _tabBar.selectedItem = _tabBar.items[0];
            _selectedItem = 0;
            [self addSubview:_tabBar];
        }
    }else{
        if (self = [super initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, TAB_MENU_VIEW_HEIGHT)]) {
            
            
        }
    }
    return self;
}

- (UITabBarItem*)createItemWithImage:(NSString*)imageName withTitle:(NSString*)title withTag:(NSInteger)tagIndex
{
    UITabBarItem *item = [[UITabBarItem alloc] initWithTitle:title image:[UIImage imageNamed:imageName] tag:tagIndex];
    return item;
}

#pragma -mark UITabBarDelegate
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    if (_selectedItem != item.tag) {
        
        _selectedItem = item.tag;
        [Global selectItemWithTag:item.tag withAnimation:NO];
        
    }
}

- (void)popToHomeWithAnimation:(BOOL)animated
{
    [Global selectItemWithTag:_selectedItem withAnimation:animated];
}

@end
