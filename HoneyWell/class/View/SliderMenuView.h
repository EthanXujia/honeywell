//
//  SliderMenuView.h
//  HoneyWell
//
//  Created by Jia.xu on 14-8-4.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SliderMenuView;
@protocol SliderMenuViewDelegate <NSObject>

@required
- (void)sliderMenuView:(SliderMenuView*)sliderMenuView withSelectIndex:(NSUInteger)index withClassName:(NSString*)className;

@end

@interface SliderMenuView : UIView
{
    
}

@property (nonatomic, weak) id<SliderMenuViewDelegate> mDelegate;

- (instancetype)initSliderMenuViewWithFrame:(CGRect)frame withItems:(NSArray*)items withDelegate:(id)obj;

@end
