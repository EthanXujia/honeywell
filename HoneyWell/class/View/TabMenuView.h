//
//  TabMenuView.h
//  HoneyWell
//
//  Created by Jia.xu on 14-8-5.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabMenuView : UIView
<UITabBarDelegate>
{
    NSInteger _selectedItem;
    IBOutlet UILabel *_airLabel;
    IBOutlet UILabel *_newsLabel;
    IBOutlet UILabel *_manualLabel;
    IBOutlet UIImageView *_airImage;
    IBOutlet UIImageView *_newsImage;
    IBOutlet UIImageView *_manualImage;
    IBOutlet UIButton *_airBtn;
    IBOutlet UIButton *_newsBtn;
    IBOutlet UIButton *_manualBtn;
}

- (instancetype)initTabMenuView;
- (void)popToHomeWithAnimation:(BOOL)animated;

@end
