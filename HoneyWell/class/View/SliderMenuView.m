//
//  SliderMenuView.m
//  HoneyWell
//
//  Created by Jia.xu on 14-8-4.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import "SliderMenuView.h"
#import "Global.h"
#import "EditLocationViewController.h"
#import "SettingViewController.h"
#import "FeedbackViewController.h"

@implementation SliderMenuView
@synthesize mDelegate=_mDelegate;


- (instancetype)initSliderMenuViewWithFrame:(CGRect)frame withItems:(NSArray*)items withDelegate:(id)obj
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor lightGrayColor];
        _mDelegate = obj;
        [self initMenuWith:items];
    }
    return self;
}

- (void)initMenuWith:(NSArray*)items
{
    if (items) {
        
        CGFloat posx = 50;
        CGFloat posy = 10;
        for (int i=0; i<items.count; i++) {
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.backgroundColor = [UIColor clearColor];
            NSString *title = nil;
            if ([items[i] isEqualToString:NSStringFromClass([EditLocationViewController class])]) {
                title = NSLocalizedString(TITLE_EDIT_LOCATION, nil);
            }else if ([items[i] isEqualToString:NSStringFromClass([SettingViewController class])]) {
                title = NSLocalizedString(TITLE_SETTING, nil);
            }else if ([items[i] isEqualToString:NSStringFromClass([FeedbackViewController class])]) {
                title = NSLocalizedString(TITLE_ADVISE_SUGGESTION, nil);
            }
            if (title) {
                [btn setTitle:title forState:UIControlStateNormal];
            }
            NSAssert(title, @"the title of the button on the slider menu is nil!");
            
            [btn setTitle:items[i] forState:UIControlStateDisabled];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            btn.titleLabel.textAlignment = NSTextAlignmentLeft;
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            btn.frame = CGRectMake(posx, posy, 100, 30);
            btn.tag = i;
            [btn addTarget:self action:@selector(touchMenuItem:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:btn];
            posy+=(30+5);
        }
        
    }
}

- (void)touchMenuItem:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    if (_mDelegate && [_mDelegate respondsToSelector:@selector(sliderMenuView:withSelectIndex:withClassName:)]) {
        NSString *className = [btn titleForState:UIControlStateDisabled];
        [_mDelegate sliderMenuView:self withSelectIndex:btn.tag withClassName:className];
    }
}


@end
