//
//  CityWeatherView.h
//  HoneyWell
//
//  Created by Jia.xu on 14-8-4.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityWeatherView : UIScrollView
{
    
    
}

@property (nonatomic, strong) UILabel *mCityLabel;

- (instancetype)initWithFrame:(CGRect)frame withWeatherInfo:(id)info;

@end
