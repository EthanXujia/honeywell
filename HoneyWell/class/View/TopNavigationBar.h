//
//  TopNavigationBar.h
//  HoneyWell
//
//  Created by Jia.xu on 14-8-5.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopNavigationBar : UIView
{
    UIButton *_leftBtn;
    UIButton *_rightBtn;
    UILabel *_titleLabel;
}

- (instancetype)initTopNavigationBar;

@end
