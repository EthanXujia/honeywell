//
//  Global.h
//  CsetMail
//
//  Created by Jia.xu on 14-7-15.
//  Copyright (c) 2014年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataManager.h"
#import "MMProgressHUD.h"
#import "CommonMarco.h"
#import "TabMenuView.h"
#import "MyHttpAPI.h"
#import "SliderMenuView.h"

// button titles
#define TITLE_LOGIN @"LOGIN"




@interface Global : NSObject

+ (void)createSettingEntity;
+ (UIViewController*)getRootViewController;
+ (UIWindow*)getWindow;
+ (UINavigationController*)getNavigationController;
+ (TabMenuView*)getTabMenuView;
+ (void)setTabMenuViewVisable:(BOOL)visable;
+ (void)cacheViewControllers:(NSArray*)array;
+ (void)selectItemWithTag:(NSInteger)tag withAnimation:(BOOL)animated;
+ (BOOL)isiOS7orLater;
+ (BOOL)isOpenGPS;
+ (void)enableGPS:(BOOL)isEnable;
+ (void)setCityName:(NSString*)cityName;
+ (NSString*)getCityName;
+ (void)openApplication:(NSString*)url;
//+ (BOOL)reachable;

+ (void)setMenuItemData:(id)data;
+ (id)getMenuItemData;

@end
