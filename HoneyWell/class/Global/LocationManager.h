//
//  LocationManager.h
//  HoneyWell
//
//  Created by Jia.xu on 14-8-7.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationManager : NSObject
<CLLocationManagerDelegate>
{
    CLLocationManager *_locationManager;
}

+ (instancetype)shareLocationManager;
- (void)startUpdateLocation;
- (void)stopUpdateLocation;

@end
