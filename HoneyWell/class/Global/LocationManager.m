//
//  LocationManager.m
//  HoneyWell
//
//  Created by Jia.xu on 14-8-7.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

#import "LocationManager.h"
#import "Global.h"

@implementation LocationManager

static LocationManager *instance = nil;
+ (instancetype)shareLocationManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[LocationManager alloc] init];
    });
    return instance;
}

- (void)enableLocation
{
    if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorized) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(TITLE_ENABLE_LOCATION_IN_SETTING, nil) delegate:nil cancelButtonTitle:NSLocalizedString(TITLE_OK, nil) otherButtonTitles:nil];
        [alert show];
        return;
    }
    [Global enableGPS:YES];
    [_locationManager startUpdatingLocation];
}

- (void)startUpdateLocation
{
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
    }
    if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorized) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(TITLE_ENABLE_LOCATION_IN_SETTING, nil) delegate:nil cancelButtonTitle:NSLocalizedString(TITLE_OK, nil) otherButtonTitles:nil];
        [alert show];
        return;
    }
    if ([Global isOpenGPS]) {
        [_locationManager startUpdatingLocation];
    }
}

- (void)stopUpdateLocation
{
    [_locationManager stopUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager
	 didUpdateLocations:(NSArray *)locations
{
    CLLocation *loc = locations.firstObject;
    if (loc) {
        [_locationManager stopUpdatingLocation];
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder  reverseGeocodeLocation:loc completionHandler:
         ^(NSArray *placemarks, NSError *error) {
             for (CLPlacemark *placemark in placemarks) {
                 if (placemark.locality.length > 0) {
                     [Global setCityName:placemark.locality];
                     [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPDATELOCATION object:nil userInfo:nil];
                     break;
                 }
             }
         }];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"locationManager didFailWithError: %@", error);
}

@end
