//
//  Global.m
//  CsetMail
//
//  Created by Jia.xu on 14-7-15.
//  Copyright (c) 2014年 apple. All rights reserved.
//

#import "Global.h"
#import "AppDelegate.h"
//#import "Reachability.h"

@implementation Global

+ (UIViewController*)getRootViewController
{
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    return app.window.rootViewController;
}

+ (UIWindow*)getWindow
{
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    return app.window;
}

+ (UINavigationController*)getNavigationController
{
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    return app.mNavigationController;
}

static TabMenuView *tabMenuView = nil;
+ (TabMenuView*)getTabMenuView
{
    if (!tabMenuView) {
        tabMenuView = [[TabMenuView alloc] initTabMenuView];
        tabMenuView.frame = CGRectMake(0, [Global getWindow].frame.size.height-tabMenuView.frame.size.height, tabMenuView.frame.size.width, tabMenuView.frame.size.height);
        //[[Global getWindow] addSubview:tabMenuView];
    }
    return tabMenuView;
}

+ (void)setTabMenuViewVisable:(BOOL)visable
{
    if (tabMenuView) {
        tabMenuView.hidden = !visable;
    }
}

+ (void)createSettingEntity
{
    NSArray *array = [DataManager arrayForEntity:[SettingEntity class] sortDescriptorsOrNil:nil];
    if (!(array && [array count] != 0)) {
        SettingEntity *entity = [DataManager createManagedInstance:[SettingEntity class]];
        [DataManager saveManagedInstances];
    }
}

static NSArray *viewControllers = nil;
+ (void)cacheViewControllers:(NSArray*)array
{
    if (viewControllers) {
        viewControllers = nil;
    }
    viewControllers = [[NSArray alloc] initWithArray:array];
}

+ (void)selectItemWithTag:(NSInteger)tag withAnimation:(BOOL)animated
{
    if (viewControllers) {
        [[Global getNavigationController] popToRootViewControllerAnimated:NO];
        if (tag != 0) {
            [[Global getNavigationController] pushViewController:viewControllers[tag] animated:NO];
        }
    }
}

+ (void)openApplication:(NSString *)url
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:url]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tmall://"]];
    }else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"www.tmall.com"]];
    }
}

+ (BOOL)isiOS7orLater {
    
    static NSUInteger _deviceSystemMajorVersion = -1;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _deviceSystemMajorVersion = [[[[[UIDevice currentDevice] systemVersion]
                                       componentsSeparatedByString:@"."] objectAtIndex:0] intValue];
    });
    
    if (_deviceSystemMajorVersion >= 7) {
        return YES;
    }
    
    return NO;
}

+ (BOOL)isOpenGPS
{
    SettingEntity *entity = nil;
    NSArray *array = [DataManager arrayForEntity:[SettingEntity class] sortDescriptorsOrNil:nil];
    if (array && [array count] != 0) {
        entity = array.firstObject;
    }
    return [entity.openGPS boolValue];
}

+ (void)enableGPS:(BOOL)isEnable
{
    SettingEntity *entity = nil;
    NSArray *array = [DataManager arrayForEntity:[SettingEntity class] sortDescriptorsOrNil:nil];
    if (array && [array count] != 0) {
        entity = array.firstObject;
        entity.openGPS = [NSNumber numberWithBool:isEnable];
        [DataManager refreshManagedInstance:entity];
        [DataManager saveManagedInstances];
    }
}

static NSString *_cityName = nil;
+ (void)setCityName:(NSString*)cityName
{
    _cityName = cityName;
}
+ (NSString*)getCityName
{
    return _cityName ? _cityName : @"---";
}
//+ (BOOL)reachable
//{	
//    Reachability *r = [Reachability reachabilityWithHostName:@"www.pretect.nl"];
//    NetworkStatus internetStatus = [r currentReachabilityStatus];
//    //NSLog(@"%d", internetStatus);
//    if(internetStatus == NotReachable) {
//		return NO;
//    }
//	return YES;
//}

static id menuItemData;
+ (void)setMenuItemData:(id)data
{
    menuItemData = data;
}

+ (id)getMenuItemData
{
    return menuItemData;
}


@end
