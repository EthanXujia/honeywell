//
//  AppDelegate.m
//  AirCleaner
//
//  Created by jia.xu on 14-7-29.
//  Copyright (c) 2014年 jia.xu. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeViewController.h"
#import "NewsViewController.h"
#import "ProductManualViewController.h"
#import "StartViewController.h"

@implementation AppDelegate

@synthesize mNavigationController = _mNavigationController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Global createSettingEntity];
    HomeViewController *homeVc = [[HomeViewController alloc] init];
    NewsViewController *newsVc = [[NewsViewController alloc] init];
    ProductManualViewController *productVc = [[ProductManualViewController alloc] init];
    [Global cacheViewControllers:@[homeVc, newsVc, productVc]];
    
    _mNavigationController = [[UINavigationController alloc] initWithRootViewController:homeVc];
    _mNavigationController.navigationBarHidden = NO;
    _mNavigationController.navigationBar.translucent = NO;
    [_mNavigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIColor colorWithRed:0 green:0 blue:0 alpha:1],
                                               UITextAttributeTextColor,
                                               [UIColor colorWithRed:0 green:0.7 blue:0.8 alpha:1],
                                               UITextAttributeTextShadowColor,
                                               [NSValue valueWithUIOffset:UIOffsetMake(0, 0)],
                                               UITextAttributeTextShadowOffset,
                                               [UIFont fontWithName:@"Arial-Bold" size:0.0], UITextAttributeFont,
                                               nil]];

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController = _mNavigationController;
    [self.window makeKeyAndVisible];
    
    if (TEST_ISFIRST) {
        StartViewController *startViewController = [[StartViewController alloc] init];
        [homeVc presentViewController:startViewController animated:NO completion:NULL];
    }
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.

}

@end
