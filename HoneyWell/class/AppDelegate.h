//
//  AppDelegate.h
//  AirCleaner
//
//  Created by jia.xu on 14-7-29.
//  Copyright (c) 2014年 jia.xu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *mNavigationController;

@end
