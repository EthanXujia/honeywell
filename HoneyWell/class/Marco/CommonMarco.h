//
//  NSObject_CommonMarco.h
//  HoneyWell
//
//  Created by Jia.xu on 14-8-4.
//  Copyright (c) 2014年 shinetechchina. All rights reserved.
//

// test data
#define TEST_CITY_NUMBERS   1
#define TEST_ISSCANNED      1
#define TEST_PRODUCTS_NUMBER    3
#define TEST_ISFIRST    0

// notification
#define NOTIFICATION_UPDATELOCATION @"NOTIFICATION_UPDATELOCATION"


// methods
#define IOS7_OR_LATER	( [[[UIDevice currentDevice] systemVersion] compare:@"7.0"] != NSOrderedAscending )

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

// Reg Ex
#define EMAIL_FORMAT    @"^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$"

#define TITLE_AIR                 @"AIR"
#define TITLE_HONEYWELL           @"HONEYWELL"
#define TITLE_MORE                @"MORE"
#define TITLE_SETTING             @"SETTING"
#define TITLE_NEWS                @"NEWS"
#define TITLE_PRODUCT_MANUALS     @"PRODUCT_MANUALS"
#define TITLE_EDIT_LOCATION       @"EDIT_LOCATION"
#define TITLE_VERSION_INFO        @"VERSION_INFO"
#define TITLE_ADVISE_SUGGESTION   @"ADVISE_SUGGESTION"
#define TITLE_ADD_CITY            @"ADD_CITY"
#define TITLE_QR_SCANNING         @"QR_SCANNING"
#define TITLE_PRODUCT_SHOPPING    @"PRODUCT_SHOPPING"
#define TITLE_INSTALLATION        @"INSTALLATION"

#define TITLE_OK                  @"OK"
#define TITLE_DONE                @"DONE"
#define TITLE_CONFIRM             @"CONFIRM"
#define TITLE_SUBMIT              @"SUBMIT"
#define TITLE_CANCEL              @"CANCEL"

#define TITLE_LOADING             @"LOADING"

#define TITLE_PLEASE_INPUT_YOUR_SUGGESTION  @"PLEASE_INPUT_YOUR_SUGGESTION"
#define TITLE_SEARCH              @"SEARCH"
#define TITLE_LOCATIOIN           @"LOCATION"
#define TITLE_ENABLE_LOCATION_IN_SETTING    @"ENABLE_LOCATION_IN_SETTING"




